# KroneckerProducts

[![pipeline status](https://gitlab.com/feather-ecosystem/KroneckerProducts/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/KroneckerProducts/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/KroneckerProducts/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/KroneckerProducts/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/KroneckerProducts/branch/master/graph/badge.svg?token=5os3IBHy3s)](https://codecov.io/gl/feather-ecosystem:core/KroneckerProducts)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/KroneckerProducts)
