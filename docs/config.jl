using KroneckerProducts, LinearAlgebra, FeaInterfaces

package_info = Dict(
    "modules" => [KroneckerProducts],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "KroneckerProducts.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/KroneckerProducts",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(KroneckerProducts, :DocTestSetup, :(using KroneckerProducts); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(KroneckerProducts, fix=true)
end
