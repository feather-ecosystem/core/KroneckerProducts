# KroneckerProducts.jl
[![pipeline status](https://gitlab.com/feather-ecosystem/KroneckerProducts/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/KroneckerProducts/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/KroneckerProducts/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/KroneckerProducts/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/KroneckerProducts/branch/master/graph/badge.svg?token=5os3IBHy3s)](https://codecov.io/gl/feather-ecosystem:core/KroneckerProducts)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/KroneckerProducts)

***

This package implements the Kronecker product matrices used extensively in the [`Feather ecosystem`](https://gitlab.com/feather-ecosystem). The implementation includes:
* products, contractions, factorizations....

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in further extensions of the functionality implemented in this package (like application to knot vectors of univariate B-splines), please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/).